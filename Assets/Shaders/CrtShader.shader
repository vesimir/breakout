﻿Shader "Custom/CrtShader"
{
	Properties
	{
		_MainTex ("Texture", 2D) = "white" {}
		_Width("Width", float) = 512
		_Height("Height", float) = 512
	}
		SubShader
		{
			// No culling or depth
			Cull Off ZWrite Off ZTest Always

		Pass
		{
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			
			#include "UnityCG.cginc"

			float _Width;
			float _Height;

			// Will return a value of 1 if the 'x' is < 'value'
			float Less(float x, float value)
			{
				return 1.0 - step(value, x);
			}

			// Will return a value of 1 if the 'x' is >= 'lower' && < 'upper'
			float Between(float x, float  lower, float upper)
			{
				return step(lower, x) * (1.0 - step(upper, x));
			}

			//	Will return a value of 1 if 'x' is >= value
			float GEqual(float x, float value)
			{
				return step(value, x);
			}

			struct appdata
			{
				float4 vertex : POSITION;
				float2 uv : TEXCOORD0;
			};

			struct v2f
			{
				float2 uv : TEXCOORD0;
				float4 vertex : SV_POSITION;
			};

			v2f vert (appdata v)
			{
				v2f o;
				o.vertex = mul(UNITY_MATRIX_MVP, v.vertex);
				o.uv = v.uv;
				return o;
			}
			
			sampler2D _MainTex;

			fixed4 frag (v2f i) : SV_Target
			{
				float brightness = 0.6;
			float contrast = 80;
				float2 uv = i.uv;
				//uv.y = -uv.y;
				//uv = uv * 0.05;

				float2 uvStep;
				uvStep.x = uv.x / (1.0 / _ScreenParams.x);
				uvStep.x = fmod(uvStep.x, 3.0);
				uvStep.y = uv.y / (1.0 / _ScreenParams.y);
				uvStep.y = fmod(uvStep.y, 3.0);

				float4 newColour = tex2D(_MainTex, uv);

				newColour.r = newColour.r * step(1.0, (Less(uvStep.x, 1.0) + Less(uvStep.y, 1.0)));
				newColour.g = newColour.g * step(1.0, (Between(uvStep.x, 1.0, 2.0) + Between(uvStep.y, 1.0, 2.0)));
				newColour.b = newColour.b * step(1.0, (GEqual(uvStep.x, 2.0) + GEqual(uvStep.y, 2.0)));

				newColour += (brightness / 255);
				newColour = newColour - contrast * (newColour - 1.0) * newColour *(newColour - 0.5);
				return newColour;
			}
			ENDCG
		}
	}
}
