﻿using UnityEngine;
using UnityEditor;

public class PowerupCreator
{
	[MenuItem("Assets/Create/Powerups/CloneExistingBalls")]
	public static void CreateCloneExistingBalls()
	{
		CreatePowerup<CloneExistingBalls>();
	}

	[MenuItem("Assets/Create/Powerups/NewBalls")]
	public static void CreateNewBalls()
	{
		CreatePowerup<NewBalls>();
	}

	[MenuItem("Assets/Create/Powerups/Revert")]
	public static void CreateRevert()
	{
		CreatePowerup<Revert>();
	}

	private static void CreatePowerup<T>()
		where T : PowerUp
	{
		System.Type type = typeof(T);

		var powerUp = ScriptableObject.CreateInstance(type);

		AssetDatabase.CreateAsset(powerUp, "Assets/PowerUps/New" + type.ToString() + ".asset");
	}
}
