using UnityEngine;

public static class GameobjectExtensions
{
	public static bool IsBrick(this GameObject gameObject)
	{
		return gameObject.CompareTag(Tags.BRICK);
	}

	public static bool IsPaddle(this GameObject gameObject)
	{
		return gameObject.CompareTag(Tags.PADDLE);
	}

	public static bool IsWall(this GameObject gameObject)
	{
		return gameObject.CompareTag(Tags.WALL);
	}

	public static bool IsBall(this GameObject gameObject)
	{
		return gameObject.CompareTag(Tags.BALL);
	}

	public static bool IsDeathArea(this GameObject gameObject)
	{
		return gameObject.CompareTag(Tags.DEATH_AREA);
	}
}
