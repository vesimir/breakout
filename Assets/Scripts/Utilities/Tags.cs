using UnityEngine;
public class Tags
{
	public const string WALL = "Wall";

	public const string PADDLE = "Paddle";

	public const string BRICK = "Brick";

	public const string BALL = "Ball";

	public const string BALL_START = "BallStart";

	public const string DEATH_AREA = "DeathArea";
}
