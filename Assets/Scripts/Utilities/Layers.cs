using UnityEngine;

public static class Layers
{
	public static readonly int Balls;

	static Layers()
	{
		int ballsLayer = LayerMask.NameToLayer("Ball");

		Debug.Assert(ballsLayer >= 0, "Balls Layer Not Defined");

		Balls = 1 << ballsLayer;
	}
}
