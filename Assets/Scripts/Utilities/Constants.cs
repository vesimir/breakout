public static class Constants
{
	public const int MAX_EXPECTED_COLLISIONS = 8;

	//Used to prvent rare cases when colliders start sleeping on each others
	public const float PHYSIC_TOLERANCE = 0.05f;
}

