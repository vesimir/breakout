using UnityEngine;

public abstract class PowerUp : ScriptableObject
{
	public int ActivateChance = 15;

	public float NextPowerupIn = 10;

	public string DisplayName;

	public abstract void Activate(GameState gameState);
}
