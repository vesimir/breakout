using UnityEngine;

public class Revert : PowerUp
{
	public override void Activate(GameState gameState)
	{
		foreach (Ball ball in gameState.Balls.GetAll())
		{
			Vector2 oldDirection = ball.GetDirection();

			Vector2 newDirection = oldDirection * -1;

			ball.SetDirection(newDirection);
		}
	}
}
