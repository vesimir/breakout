using System.Collections.Generic;
using UnityEngine;

public class CloneExistingBalls : PowerUp
{
	[Header("Clone Existing Balls specific")]
	[SerializeField]
	int CloneCount;

	public override void Activate(GameState gameState)
	{
		List<Ball> newBalls = new List<Ball>();

		foreach (Ball existingBall in gameState.Balls.GetAll())
		{
			for (int i = 0; i < CloneCount; i++)
			{
				Ball newBall = BallFactory.GetInstance(existingBall);
				newBall.State = BallState.InGame;

				Vector3 position = existingBall.GetPosition();
				newBall.SetPosition(position);

				Vector2 radnomDirection = Random.insideUnitCircle;
				newBall.SetDirection(radnomDirection);

				newBalls.Add(newBall);
			}
		}

		gameState.Balls.AddRange(newBalls);
	}
}
