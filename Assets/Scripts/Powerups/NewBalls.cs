using UnityEngine;

public class NewBalls : PowerUp
{
	[Header("New Balls specific")]
	[SerializeField]
	Ball[] _balls;

	public override void Activate(GameState gameState)
	{
		foreach (Ball prefab in _balls)
		{
			Ball ball = BallFactory.GetInstance(prefab);

			gameState.Balls.Add(ball);
		}
	}
}
