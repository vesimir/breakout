using UnityEngine;

[System.Serializable]
public class Effects
{
	public ParticleSystem BrickHit { get { return _brickHit; } }

	public ParticleSystem WallHit { get { return _wallHit; } }

	public ParticleSystem PaddleHit { get { return _paddleHit; } }

	public ParticleSystem BallDestroyed { get { return _ballDestroyed; } }

	[SerializeField]
	ParticleSystem _brickHit;

	[SerializeField]
	ParticleSystem _wallHit;

	[SerializeField]
	ParticleSystem _paddleHit;

	[SerializeField]
	ParticleSystem _ballDestroyed;
}
