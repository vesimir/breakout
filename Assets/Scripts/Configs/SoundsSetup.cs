using UnityEngine;

[System.Serializable]
public class SoundsSetup
{
	public AudioClip BrickHit { get { return _brickHit; } }

	public AudioClip WallHit { get { return _wallHit; } }

	public AudioClip PaddleHit { get { return _paddleHit; } }

	public AudioClip BallLost { get { return _ballLost; } }

	public AudioClip LifeLost { get { return _lifeLost; } }

	public AudioClip PlayerDied { get { return _playerDied; } }

	[SerializeField]
	AudioClip _brickHit;

	[SerializeField]
	AudioClip _wallHit;

	[SerializeField]
	AudioClip _paddleHit;

	[SerializeField]
	AudioClip _ballLost;

	[SerializeField]
	AudioClip _lifeLost;

	[SerializeField]
	AudioClip _playerDied;
}
