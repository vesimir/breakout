using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class SoundsManager : Manager, BallObserver
{
	AudioSource _AudioSource;

	SoundsSetup _SoundSetup;

	public SoundsManager(Game owner, GameState gameState)
		: base(owner, gameState)
	{
		_AudioSource = GetAudioSource();

		_SoundSetup = Owner.Sounds;

		GameData.Lives.Losted += PlayLifeLost;

		GameData.Balls.Removed += OnBallLost;
	}

	private AudioSource GetAudioSource()
	{
		 var audioSource = Owner.GetComponent<AudioSource>();

		Debug.Assert(audioSource != null, "No audio source defined", Owner);
		return audioSource;
	}

	private void PlayLifeLost(int obj)
	{
		PlayOneShot(_SoundSetup.LifeLost);
	}

	private void PlayOneShot(AudioClip audioClip)
	{
		_AudioSource.PlayOneShot(audioClip);
	}

	void BallObserver.OnBallCollision(RaycastHit2D hitInfo)
	{
		GameObject collidingGameObject = hitInfo.collider.gameObject;

		if (collidingGameObject.IsBrick())
		{
			PlayOneShot(_SoundSetup.BrickHit);
		}
		else if (collidingGameObject.IsPaddle())
		{
			PlayOneShot(_SoundSetup.PaddleHit);
		}
		else if (collidingGameObject.IsWall())
		{
			PlayOneShot(_SoundSetup.WallHit);
		}
	}

	void OnBallLost(Ball ball)
	{
		PlayOneShot(_SoundSetup.BallLost);
	}
}
