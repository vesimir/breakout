using System.Collections.ObjectModel;
using UnityEngine;

public class PaddleManager : Manager, Updateable
{
	RaycastHit2D[] _hitsInfo;

	ReadOnlyCollection<Paddle> _paddles;

	public PaddleManager(Game owner, GameState gameState)
	: base(owner, gameState)
	{
		_hitsInfo = new RaycastHit2D[8];

		_paddles = GameData.Paddles.GetAll();
	}

	void Updateable.Update()
	{
		InputDirection inputDirection = GameData.Input.Direction;

		if (inputDirection == InputDirection.None)
			return;

		for (int i = 0, count = _paddles.Count; i < count; i++)
		{
			Paddle paddle = _paddles[i];

			Move(paddle, inputDirection);
		}
	}

	private void Move(Paddle paddle, InputDirection inputDirection)
	{
		Vector2 oldPosition = paddle.GetPosition();

		Vector2 direction = inputDirection == InputDirection.Left ? paddle.transform.right * -1 : paddle.transform.right;

		float distance = paddle.Speed * Time.deltaTime;

		RaycastHit2D info = GetFirstValidHit(paddle, oldPosition, direction, distance);

		if (info.collider != null)
		{
			bool hasHitBall = info.collider.gameObject.IsBall();
			if (hasHitBall)
			{
				DestroyBallIfHitBySideOfPaddle(paddle, info);
			}

			return;
		}

		Vector2 newPosition = oldPosition + direction * distance;

		paddle.SetPostion(newPosition);
	}

	private void DestroyBallIfHitBySideOfPaddle(Paddle paddle, RaycastHit2D info)
	{
		Vector3 toCollisionPoint = info.point - info.centroid;

		float halfHeight = (paddle.GetColliderSize().y * 0.5f);
		halfHeight -= Constants.PHYSIC_TOLERANCE;

		Vector3 toPaddleTop = paddle.transform.up;

		float dot = Vector3.Dot(toPaddleTop, toCollisionPoint);

		bool hitFromSide = dot > -halfHeight && dot < halfHeight;

		if (hitFromSide)
		{
			Ball ball = info.collider.GetComponent<Ball>();

			GameData.Balls.Remove(ball);
			BallFactory.Destroy(ball);
		}
	}

	private RaycastHit2D GetFirstValidHit(Paddle paddle, Vector2 oldPosition, Vector2 direction, float distance)
	{
		RaycastHit2D info = default(RaycastHit2D);

		Vector2 colliderSize = paddle.GetColliderSize();

		float zRotation = paddle.transform.rotation.eulerAngles.z;

		int numOfHits = Physics2D.BoxCastNonAlloc(oldPosition, colliderSize, zRotation, direction, _hitsInfo, distance + Constants.PHYSIC_TOLERANCE);

		for (int i = 0, count = numOfHits; i < count; i++)
		{
			RaycastHit2D hitInfo = _hitsInfo[i];

			GameObject collidingGO = hitInfo.collider.gameObject;

			if (collidingGO == paddle.gameObject)
				continue;

			info = hitInfo;

			break;
		}

		return info;
	}
}
