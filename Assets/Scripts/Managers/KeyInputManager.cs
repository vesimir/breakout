using UnityEngine;
using UnityInput = UnityEngine.Input;

public class KeyInputManager : Manager, Updateable
{
	public KeyInputManager(Game owner, GameState gameState)
	: base(owner, gameState)
	{
	}

	void Updateable.Update()
	{
		GameData.Input.Direction = GetInputDirection();

		GameData.Input.ShootBall = WantShootBall();
	}

	private bool WantShootBall()
	{
		return UnityInput.GetKeyUp(KeyCode.Space);
	}

	private InputDirection GetInputDirection()
	{
		if (UnityInput.GetKey(KeyCode.LeftArrow))
			return InputDirection.Left;

		if (UnityInput.GetKey(KeyCode.RightArrow))
			return InputDirection.Right;

		return InputDirection.None;
	}
}
