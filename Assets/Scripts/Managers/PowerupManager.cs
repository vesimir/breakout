using UnityEngine;

public class PowerupManager : Manager
{
	PowerUp[] _powerUps;

	int _totalProbability;

	float _nextAllowedActivationTime;

	public PowerupManager(Game owner, GameState gameState)
		: base(owner, gameState)
	{
		_powerUps = Owner.PowerUps.Definition;

		foreach (PowerUp powerUp in _powerUps)
		{
			_totalProbability += powerUp.ActivateChance;
		}

		GameData.Bricks.BrickDestroyed += OnBrickDestroyed;
	}

	private void OnBrickDestroyed(Brick brick, bool allBricksDestroyed)
	{
		if (allBricksDestroyed)
			return;

		if (_powerUps.Length == 0)
			return;

		if (_nextAllowedActivationTime > Time.timeSinceLevelLoad)
			return;

		PowerUp powerUp = GetPowerUp();

		powerUp.Activate(GameData);

		Owner.Hud.PushToConsole(powerUp.DisplayName);

		_nextAllowedActivationTime = Time.timeSinceLevelLoad + powerUp.NextPowerupIn;
	}

	private PowerUp GetPowerUp()
	{
		int randomProbability = Random.Range(0, _totalProbability);

		foreach (PowerUp powerUp in _powerUps)
		{
			randomProbability -= powerUp.ActivateChance;

			if (randomProbability > 0)
				continue;

			return powerUp;
		}

		return null;
	}
}
