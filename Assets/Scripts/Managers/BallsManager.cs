using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class BallsManager : Manager, Subject, Updateable
{
	const float PADDLE_REFLECTION_MAX_ANGLE = 85f;

	Transform _ballStart;

	List<BallObserver> _observers;

	ReadOnlyCollection<Ball> _balls;

	Collider2D[] _overlapBalls;

	RaycastHit2D[] _hitsInfo;

	public BallsManager(Game owner, GameState gameState)
		: base(owner, gameState)
	{
		_observers = new List<BallObserver>();

		_overlapBalls = new Collider2D[Constants.MAX_EXPECTED_COLLISIONS];

		_hitsInfo = new RaycastHit2D[Constants.MAX_EXPECTED_COLLISIONS];

		_balls = GameData.Balls.GetAll();

		_ballStart = GetBallStart();

		RegisterCallbacks();

		AddDefaultBall();
	}

	public void StartWaitingBallIfAny()
	{
		Ball waitingBall = GameData.Balls.GetWaitingBall();

		if (waitingBall == null)
			return;

		waitingBall.State = BallState.InGame;

		Vector2 randomDirection = GetRandomDirection();

		waitingBall.SetDirection(randomDirection);
	}

	void Subject.AddObserver(BallObserver observer)
	{
		_observers.Add(observer);
	}

	void Updateable.Update()
	{
		MoveAllBallsInGame();

		if (GameData.Input.ShootBall == false)
		{
			AllignWaitingBallToStartPosition();

			return;
		}

		StartWaitingBallIfAny();
	}

	private Transform GetBallStart()
	{
		Transform ballStart = GameObject.FindGameObjectWithTag(Tags.BALL_START).transform;

		Debug.Assert(ballStart != null, "No ball start defined for scene");

		return ballStart;
	}

	private void RegisterCallbacks()
	{
		GameData.Lives.Losted += OnLiveLost;

		GameData.Bricks.BrickDestroyed += OnBrickDestroyed;

		GameData.Balls.Added += OnBallAdded;
	}

	private void OnBallAdded(Ball ball)
	{
		if (ball.State != BallState.WatingForActivation)
			return;

		ActivateBall(ball);
	}

	private void OnBrickDestroyed(Brick brick, bool allBrickDestroyed)
	{
		if (allBrickDestroyed == false)
			return;

		Owner.StartCoroutine(ResetBallsOnEndOfFrame());
	}

	private IEnumerator ResetBallsOnEndOfFrame()
	{
		yield return new WaitForEndOfFrame();

		for (int i = _balls.Count - 1; i >= 0; i--)
		{
			Ball ball = _balls[i];

			if (i == 0)
			{
				ball.State = BallState.WaitingForStart;

				break;
			}

			DestroyBall(ball);
		}
	}

	private void OnLiveLost(int livesCount)
	{
		AddDefaultBall();
	}

	private void AddDefaultBall()
	{
		Ball ball = BallFactory.GetInstance(Owner.StartingBall);

		GameData.Balls.Add(ball);
	}

	private void ActivateBall(Ball ball)
	{
		StartWaitingBallIfAny();

		ball.State = BallState.WaitingForStart;

		ball.SetPosition(_ballStart.position);
	}

	private void DestroyBall(Ball ball)
	{
		BallFactory.Destroy(ball);

		GameData.Balls.Remove(ball);
	}

	private Vector2 GetRandomDirection()
	{
		Vector2 left = _ballStart.up - _ballStart.right;
		Vector2 right = _ballStart.up + _ballStart.right;

		bool startLeft = Random.value > 0.5f;

		Vector2 randomDirection = startLeft ? left : right;
		return randomDirection;
	}

	private void NotifyCollison(RaycastHit2D hitInfo)
	{
		foreach (BallObserver observer in _observers)
		{
			observer.OnBallCollision(hitInfo);
		}
	}

	private void AllignWaitingBallToStartPosition()
	{
		Ball WaitingBall = GameData.Balls.GetWaitingBall();

		if (WaitingBall != null)
		{
			WaitingBall.transform.position = _ballStart.position;
		}
	}

	private void MoveAllBallsInGame()
	{
		for (int i = _balls.Count - 1; i >= 0; i--)
		{
			Ball ball = _balls[i];

			if (ball.State != BallState.InGame)
				continue;

			Move(ball);
		}
	}

	void Move(Ball ball)
	{
		Vector2 actualPosition = ball.GetPosition();

		Vector2 direction = ball.GetDirection();

		float distance = ball.GetSpeed() * Time.deltaTime;

		float radius = ball.GetRadius();

		RaycastHit2D hitInfo = GetFirstValidHitInfo(ball, actualPosition, direction, distance, radius);

		bool hitAnything = hitInfo.collider != null;

		if (hitAnything == false)
		{
			Vector2 desiredPosition = actualPosition + direction * distance;

			ball.SetPosition(desiredPosition);

			return;
		}

		bool shouldBeDestroyed = hitInfo.collider.gameObject.IsDeathArea();

		if (shouldBeDestroyed)
		{
			DestroyBall(ball);

			return;
		}

		bool hitPaddle = hitInfo.collider.gameObject.IsPaddle();

		if (hitPaddle)
		{
			ReflectFromPaddle(ball, actualPosition, hitInfo);
		}
		else
		{
			Vector2 newDirection = ReflectFromObstacle(hitInfo, ball, actualPosition, direction, distance);

			ChangeAlsoCollidingObjectIfIsBall(hitInfo, newDirection);
		}

		NotifyCollison(hitInfo);
	}

	private static void ReflectFromPaddle(Ball ball, Vector2 actualPosition, RaycastHit2D hitInfo)
	{
		Paddle paddle = hitInfo.collider.GetComponent<Paddle>();

		float halfWidth = paddle.GetColliderSize().x / 2f;
		Vector2 toRightSide = paddle.transform.right * halfWidth;

		Vector2 toCollision = hitInfo.point - paddle.GetPosition();

		float rightDot = Vector2.Dot(toRightSide, toCollision);

		bool hitTop = Vector2.Dot(toCollision, paddle.transform.up) > 0f;

		Vector2 newDirecion = GetNewDirection(paddle, hitTop, rightDot);
		Vector2 newPosition = actualPosition + newDirecion * Constants.PHYSIC_TOLERANCE;

		ball.SetPosition(newPosition);
		ball.SetDirection(newDirecion);
	}

	private static Vector2 GetNewDirection(Paddle paddle, bool hitTop, float rightDot)
	{
		Vector2 collsionNormal = hitTop ? paddle.transform.up : paddle.transform.up * -1;

		float angle = rightDot * PADDLE_REFLECTION_MAX_ANGLE;
		if (hitTop == false)
		{
			angle *= -1;
		}

		Quaternion rotation = Quaternion.AngleAxis(angle, Vector3.back);

		Vector2 newDirecion = rotation * collsionNormal;
		return newDirecion;
	}

	private Vector2 ReflectFromObstacle(RaycastHit2D hitInfo, Ball ball, Vector2 actualPosition, Vector2 direction, float distance)
	{
		Vector2 newDirection = Vector2.Reflect(direction, hitInfo.normal);

		newDirection = AddRandomRotationIfReflectingSameWayBack(direction, newDirection);

		Vector2 newPosition = CalculateNewPosition(hitInfo, actualPosition, distance, newDirection);

		ball.SetPosition(newPosition);

		ball.SetDirection(newDirection);

		return newDirection;
	}

	private Vector2 CalculateNewPosition(RaycastHit2D hitInfo, Vector2 actualPosition, float distance, Vector2 newDirection)
	{
		float remainingDistance = (distance - Vector2.Distance(hitInfo.centroid, actualPosition));

		Vector2 offset = newDirection * remainingDistance;

		Vector2 startPosition = hitInfo.centroid + (hitInfo.normal * Constants.PHYSIC_TOLERANCE);

		Vector2 newPosition = startPosition + offset;
		return newPosition;
	}

	private Vector2 AddRandomRotationIfReflectingSameWayBack(Vector2 direction, Vector2 newDirection)
	{
		if (Vector2.Dot(direction, newDirection) < -0.98f)
		{
			float randomZRotation = Random.Range(-4f, 4f);

			Quaternion randomRotation = Quaternion.AngleAxis(randomZRotation, Vector3.forward);

			newDirection = randomRotation * newDirection;
		}

		return newDirection;
	}

	private void ChangeAlsoCollidingObjectIfIsBall(RaycastHit2D hitInfo, Vector2 newDirection)
	{
		bool hitBall = hitInfo.collider.gameObject.IsBall();

		if (hitBall)
		{
			var collidingBall = hitInfo.collider.gameObject.GetComponent<Ball>();

			collidingBall.SetDirection(newDirection * -1f);
		}
	}

	private RaycastHit2D GetFirstValidHitInfo(Ball ball, Vector2 actualPosition, Vector2 direction, float distance, float radius)
	{
		float overlapRadius = radius - 0.01f;
		Physics2D.OverlapCircleNonAlloc(actualPosition, overlapRadius, _overlapBalls, Layers.Balls);

		RaycastHit2D hitInfo = default(RaycastHit2D);

		float rayDistance = distance + Constants.PHYSIC_TOLERANCE;
		int numOfHits = Physics2D.CircleCastNonAlloc(actualPosition, radius, direction, _hitsInfo, rayDistance);

		for (int i = 0, count = numOfHits; i < count; i++)
		{
			RaycastHit2D hit = _hitsInfo[i];

			GameObject collidingGo = hit.collider.gameObject;

			if (collidingGo == ball.gameObject)
				continue;

			if (collidingGo.IsBall())
			{
				bool wasOverlaping = System.Array.Exists(_overlapBalls, Equals(collidingGo));

				if (wasOverlaping)
					continue;
			}

			hitInfo = hit;
		}

		return hitInfo;
	}

	private static System.Predicate<Collider2D> Equals(GameObject collidingGo)
	{
		return op => op != null && op.gameObject == collidingGo;
	}
}
