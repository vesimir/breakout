
public abstract class Manager
{
	protected Game Owner;

	protected GameState GameData;

	protected Manager(Game owner, GameState gameState)
	{
		Owner = owner;

		GameData = gameState;
	}
}
