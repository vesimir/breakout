public interface Subject
{
	void AddObserver(BallObserver observer);
}
