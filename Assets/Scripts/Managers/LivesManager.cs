public class LivesManager : Manager
{
	const int LIVE_LOST = 1;

	public LivesManager(Game owner, GameState gameState)
		: base(owner, gameState)
	{
		GameData.Lives.Changed += OnLivesChanged;

		GameData.Lives.Death += OnDeath;

		GameData.Balls.Removed += OnBallLost;

		GameData.Bricks.BrickDestroyed += OnBrickDestroyed;

		ResetLives();
	}

	private void OnLivesChanged(int newValue)
	{
		Owner.Hud.DisplayLives(newValue);
	}

	private void OnBrickDestroyed(Brick brick, bool allBrickDestroyed)
	{
		if (allBrickDestroyed == false)
			return;

		ResetLives();
	}
	
	private void ResetLives()
	{
		GameData.Lives.Set(Owner.Lives);
	}

	private void OnDeath()
	{
		Owner.Hud.PushToConsole("Game Over");
	}

	private void OnBallLost(Ball ball)
	{
		if (GameData.Balls.Any())
			return;

		GameData.Lives.Substract(LIVE_LOST);
	}
}
