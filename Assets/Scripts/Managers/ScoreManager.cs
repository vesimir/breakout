public class ScoreManager : Manager
{
	const int SCORE_INCREMENT = 100;

	const int SCORE_PER_LIFE = 1000;

	int _actualScoreIncrement;

	public ScoreManager(Game owner, GameState gameState)
		: base(owner, gameState)
	{
		gameState.Score.Changed += OnScoreChanged;

		gameState.Lives.Losted += ResetIncrement;

		gameState.Bricks.BrickDestroyed += OnBrickDestroyed;
	}

	private void OnScoreChanged(int newScore)
	{
		Owner.Hud.DisplayScore(newScore);
	}

	private void OnBrickDestroyed(Brick brick, bool allBricksDestoryed)
	{
		AddScoreForDestroyedBrick();

		if (allBricksDestoryed == false)
			return;

		AddScoreForWiningGame();
	}

	private void AddScoreForWiningGame()
	{
		_actualScoreIncrement = 0;

		int scoreAddon = GameData.Lives.Get() * SCORE_PER_LIFE;

		GameData.Score.Add(scoreAddon);
	}

	private void AddScoreForDestroyedBrick()
	{
		_actualScoreIncrement += SCORE_INCREMENT;

		GameData.Score.Add(_actualScoreIncrement);
	}

	private void ResetIncrement(int lifesLost)
	{
		_actualScoreIncrement = 0;
	}

}
