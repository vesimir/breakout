using UnityEngine;

public interface BallObserver
{
	void OnBallCollision(RaycastHit2D hitInfo);
}
