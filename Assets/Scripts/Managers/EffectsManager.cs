using UnityEngine;

public class EffectsManager : Manager, BallObserver
{
	//TODO: add max pool size
	//TODO: different sizes for different pools
	const int INITIAL_POOL_SIZE = 1;

	EffectsPool _wallHitEffectsPool;

	EffectsPool _paddleHitEffectsPool;

	EffectsPool _brickHitEffectsPool;

	EffectsPool _ballDestroyedEffectPool;


	public EffectsManager(Game owner, GameState gameState)
		: base(owner, gameState)
	{
		_wallHitEffectsPool = new EffectsPool(owner.Effects.WallHit, INITIAL_POOL_SIZE);

		_paddleHitEffectsPool = new EffectsPool(owner.Effects.PaddleHit, INITIAL_POOL_SIZE);

		_brickHitEffectsPool = new EffectsPool(owner.Effects.BrickHit, INITIAL_POOL_SIZE);

		_ballDestroyedEffectPool = new EffectsPool(owner.Effects.BallDestroyed, INITIAL_POOL_SIZE);

		GameData.Balls.Removed += OnBallRemoved;
	}

	private void OnBallRemoved(Ball ball)
	{
		ParticleSystem effect = _ballDestroyedEffectPool.Get();

		PlayEffect(effect, ball.GetPosition());
	}

	void BallObserver.OnBallCollision(RaycastHit2D hitInfo)
	{
		GameObject collidingGameObject = hitInfo.collider.gameObject;

		ParticleSystem effect = GetEffect(collidingGameObject);

		PlayEffect(effect, hitInfo.point);
	}

	private void PlayEffect(ParticleSystem effect, Vector2 position)
	{
		if (effect == null)
			return;

		effect.transform.position = new Vector3(position.x, position.y, -5f);

		effect.gameObject.SetActive(false);

		effect.gameObject.SetActive(true);
	}

	private ParticleSystem GetEffect(GameObject collidingGameObject)
	{
		if (collidingGameObject.IsBrick())
			return _brickHitEffectsPool.Get();
		
		if (collidingGameObject.IsPaddle())
			return _paddleHitEffectsPool.Get();
		
		if (collidingGameObject.IsWall())	
			return _wallHitEffectsPool.Get();
	
		return null;
	}
}
