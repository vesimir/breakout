using System.Collections.Generic;
using UnityEngine;

public class EffectsPool
{
	ParticleSystem _prefab;

	List<ParticleSystem> _effectPool;

	public EffectsPool(ParticleSystem prefab, int startSize)
	{
		_prefab = prefab;

		_effectPool = new List<ParticleSystem>(startSize);

		PreinitPool(prefab, startSize);
	}

	public ParticleSystem Get()
	{
		foreach (ParticleSystem effect in _effectPool)
		{
			if (effect.gameObject.activeSelf && effect.isStopped == false)
				continue;

			return effect;
		}

		ParticleSystem particleSystem = Create(_prefab);

		_effectPool.Add(particleSystem);

		return particleSystem;
	}

	private void PreinitPool(ParticleSystem prefab, int startSize)
	{
		for (int i = 0; i < startSize; i++)
		{
			ParticleSystem particleSystem = Create(prefab);

			_effectPool.Add(particleSystem);
		}
	}

	private ParticleSystem Create(ParticleSystem prefab)
	{
		ParticleSystem particleSystem = (ParticleSystem)Object.Instantiate(prefab);

		particleSystem.gameObject.SetActive(false);

		return particleSystem;
	}
}
