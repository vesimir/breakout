using System.Collections;
using System.Collections.ObjectModel;
using UnityEngine;

public class BricksManager : Manager, BallObserver
{
	WaitUntil _waitUntilBallsAreReseted;

	public BricksManager(Game owner, GameState gameState)
		: base(owner, gameState)
	{
		GameData.Bricks.BrickDestroyed += OnBrickDestroyed;

		InitWaitUntilBallsAreReseted();
	}

	void BallObserver.OnBallCollision(RaycastHit2D hitInfo)
	{
		GameObject collidingObject = hitInfo.collider.gameObject;

		if (collidingObject.IsBrick() == false)
			return;

		Brick brick = collidingObject.GetComponent<Brick>();

		GameData.Bricks.Destroy(brick);
	}

	private void InitWaitUntilBallsAreReseted()
	{
		ReadOnlyCollection<Ball> balls = GameData.Balls.GetAll();

		_waitUntilBallsAreReseted = new WaitUntil(() => balls.Count == 1 && balls[0].State == BallState.WaitingForStart);
	}

	private void OnBrickDestroyed(Brick brick, bool allBricksDestoryed)
	{
		if (allBricksDestoryed == false)
			return;

		Owner.StartCoroutine(EnableAllBricksNextFrame());
	}

	private IEnumerator EnableAllBricksNextFrame()
	{
		yield return _waitUntilBallsAreReseted;

		GameData.Bricks.EnableAll();
	}
}
