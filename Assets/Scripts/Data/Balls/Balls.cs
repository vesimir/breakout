using System.Collections.Generic;
using System.Collections.ObjectModel;

public class Balls
{
	public System.Action<Ball> Added;

	public System.Action<Ball> Removed;

	List<Ball> _balls;

	public Balls()
	{
		_balls = new List<Ball>();

		Added += FallBackCall;

		Removed += FallBackCall;
	}

	public void Add(Ball ball)
	{
		_balls.Add(ball);

		Added(ball);
	}

	public void AddRange(List<Ball> newBalls)
	{
		_balls.AddRange(newBalls);
	}

	public void Remove(Ball ball)
	{
		_balls.Remove(ball);

		Removed(ball);
	}

	public ReadOnlyCollection<Ball> GetAll()
	{
		return _balls.AsReadOnly();
	}

	public Ball GetWaitingBall()
	{
		return _balls.Find(b => b.State == BallState.WaitingForStart);
	}

	public bool Any()
	{
		return _balls.Count > 0;
	}

	private void FallBackCall(Ball ball)
	{
	}
}
