using System;
using System.Linq;
using System.Collections.Generic;
using UnityEngine;

public static class BallFactory
{
	public static Ball GetInstance(Ball prefab)
	{
		return (Ball)GameObject.Instantiate(prefab);
	}

	public static void Destroy(Ball ball)
	{
		GameObject.Destroy(ball.gameObject);
	}
}
