using System.Collections.Generic;
using UnityEngine;

public class Lives
{
	const int MIN_LIVES = 0;

	public System.Action<int> Losted;	
	public System.Action<int> Changed;

	public System.Action Death;

	int _value;

	public int Get()
	{
		return _value;
	}

	public void Set(int value)
	{
		_value = value;

		NotifyChange();

		if (IsDead())
		{
			NotifyDead();
		}
	}

	private void NotifyDead()
	{
		if (Death != null)
			Death();
	}

	public void Substract(int value)
	{
		_value = Mathf.Max(MIN_LIVES, _value - value);

		NotifyChange();

		if (IsDead() == false)
		{
			NotifyLost(value);
		}
		else
		{
			NotifyDead();
		}
	}

	public bool IsDead()
	{
		return _value == MIN_LIVES;
	}

	private void NotifyLost(int value)
	{
		if (Losted != null)
			Losted(value);
	}

	private void NotifyChange()
	{
		if (Changed != null)
		{
			Changed(_value);
		}
	}
}
