﻿using UnityEngine;

public class GameState
{
	public readonly Balls Balls;

	public readonly Score Score;

	public readonly Lives Lives;

	public readonly Bricks Bricks;

	public readonly Input Input;

	public readonly Paddles Paddles;

	public GameState()
	{
		Balls = new Balls();

		Score = new Score();

		Lives = new Lives();

		Bricks = new Bricks();

		Input = new Input();

		Paddles = new Paddles();
	}
}

