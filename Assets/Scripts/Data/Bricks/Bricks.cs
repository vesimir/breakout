using System.Collections.Generic;
using UnityEngine;

public class Bricks
{
	public System.Action<Brick, bool> BrickDestroyed;

	Brick[] _bricks;

	public Bricks()
	{
		_bricks = GetAllBricks();
	}

	private Brick[] GetAllBricks()
	{
		GameObject[] bricksGOs = GameObject.FindGameObjectsWithTag(Tags.BRICK);

		Debug.Assert(bricksGOs.Length > 0, "No bricks defined in scene");

		return System.Array.ConvertAll(bricksGOs, go => go.GetComponent<Brick>());
	}

	public void EnableAll()
	{
		foreach (Brick brick in _bricks)
		{
			if (brick.State == BrickState.Normal)
				continue;

			brick.Enable();
		}
	}

	public void Destroy(Brick brick)
	{
		bool allBricksDestoryed = System.Array.Exists(_bricks, b => b.State == BrickState.Normal && b != brick) == false;

		if (allBricksDestoryed == false)
		{
			brick.Disable();
		}

		if (BrickDestroyed != null)
		{
			BrickDestroyed(brick, allBricksDestoryed);
		}
	}
}
