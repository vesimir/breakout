using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;

public class Paddles
{
	List<Paddle> _paddles;

	public Paddles()
	{
		_paddles = new List<Paddle>();

		GameObject[] paddlesGOs = GameObject.FindGameObjectsWithTag(Tags.PADDLE);

		Debug.Assert(paddlesGOs.Length > 0, "No paddles defined in scene");

		foreach (GameObject paddleGO in paddlesGOs)
		{
			Paddle paddle = paddleGO.GetComponent<Paddle>();

			_paddles.Add(paddle);
		}
	}

	public ReadOnlyCollection<Paddle> GetAll()
	{
		return _paddles.AsReadOnly();
	}
}
