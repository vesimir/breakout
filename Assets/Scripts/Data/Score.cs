
public class Score
{
	public System.Action<int> Changed;

	int _value;

	public int Get()
	{
		return _value;
	}

	public void Add(int value)
	{
		_value += value;

		NotifyChange();
	}

	private void NotifyChange()
	{
		if (Changed != null)
		{
			Changed(_value);
		}
	}
}
