using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Hud : MonoBehaviour
{
	const float MASSAGE_DURATION = 1f;

	[SerializeField]
	Text _score;

	[SerializeField]
	Text _lives;

	[SerializeField]
	Text _console;

	IEnumerator _consoleRoutine;

	Queue<string> _consoleQueue;

	WaitForSeconds _waitForMessageDuration;

	void Awake()
	{
		_console.text = string.Empty;

		_consoleQueue = new Queue<string>(10);

		_waitForMessageDuration = new WaitForSeconds(MASSAGE_DURATION);

		StartCoroutine(ConsoleCoroutine());
	}

	public void DisplayScore(int value)
	{ 
		_score.text = value.ToString();
	}

	public void DisplayLives(int value)
	{
		_lives.text = value.ToString();
	}

	public void PushToConsole(string message)
	{
		_consoleQueue.Enqueue(message);
	}

	IEnumerator ConsoleCoroutine()
	{
		while (true)
		{
			bool hasSomethingToDisplay = _consoleQueue.Count > 0;

			if (hasSomethingToDisplay == false)
			{
				yield return null;

				continue;
			}

			var massage = _consoleQueue.Dequeue();

			_console.text = massage;

			yield return _waitForMessageDuration;

			_console.text = string.Empty;
		}
	}
}
