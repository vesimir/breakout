using UnityEngine;

[RequireComponent(typeof(CircleCollider2D))]
public class Ball : MonoBehaviour
{
	[SerializeField]
	float _speed;

	public BallState State { get; set; }

	Vector2 _direction;

	CircleCollider2D _collider;

	void Awake()
	{
		_direction = transform.up;

		_collider = GetComponent<CircleCollider2D>();
	}

	public Vector2 GetPosition()
	{
		return transform.position;
	}

	public Vector2 GetDirection()
	{
		return _direction;
	}

	public float GetSpeed()
	{
		return _speed;
	}

	public float GetRadius()
	{
		return _collider.radius;
	}

	public void SetPosition(Vector2 desiredPosition)
	{
		transform.position = desiredPosition;
	}

	public void SetDirection(Vector2 newDirection)
	{
		newDirection.Normalize();

		_direction = newDirection;
	}
}
