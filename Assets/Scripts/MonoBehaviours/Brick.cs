using UnityEngine;
using System.Collections;

[RequireComponent(typeof(Animator))]
[RequireComponent(typeof(BoxCollider2D))]
public class Brick : MonoBehaviour
{
	const string SHOW_TRIGGER = "ShowTrigger";
	const string HIDE_TRIGGER = "HideTrigger";

	public BrickState State { get; private set; }

	Animator _animator;

	BoxCollider2D _collider;

	WaitWhile _waitWhileIsInTransiton;

	WaitWhile _waitWhileAnimationIsPlaying;

	void Awake()
	{
		_animator = GetComponent<Animator>();

		_collider = GetComponent<BoxCollider2D>();

		_waitWhileIsInTransiton = new WaitWhile(() => _animator.IsInTransition(0));

		_waitWhileAnimationIsPlaying = new WaitWhile(() => IsAnimationPlaying());
	}

	public void Enable()
	{
		State = BrickState.Normal;

		gameObject.SetActive(true);

		StartCoroutine(EnableCoroutine());
	}

	public void Disable()
	{
		State = BrickState.Destroyed;

		StartCoroutine(DisableCoroutine());
	}

	public bool IsActive()
	{
		return gameObject.activeSelf;
	}

	IEnumerator EnableCoroutine()
	{
		_animator.SetTrigger(SHOW_TRIGGER);

		yield return _waitWhileIsInTransiton;

		yield return _waitWhileAnimationIsPlaying;

		yield return _waitWhileIsInTransiton;

		_collider.enabled = true;

		transform.localScale = Vector3.one;
	}

	IEnumerator DisableCoroutine()
	{
		_collider.enabled = false;

		_animator.SetTrigger(HIDE_TRIGGER);

		yield return _waitWhileIsInTransiton;

		yield return _waitWhileAnimationIsPlaying;

		gameObject.SetActive(false);
	}

	bool IsAnimationPlaying()
	{
		AnimatorClipInfo[] clipsInfo = _animator.GetCurrentAnimatorClipInfo(0);

		return clipsInfo.Length > 0;
	}
}
