﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;

public class Game : MonoBehaviour 
{
	public int Lives;

	public Ball StartingBall;

	public PowerUps PowerUps;

	public Effects Effects;

	public SoundsSetup Sounds;

	public Hud Hud;

	Manager[] _managers;

	List<Updateable> _updateables;

	void Awake()
	{
		InitialiseManagers();

		RegisterBallObservers();

		RegisterUpdatables();
	}

	void Update()
	{
		for (int i = 0, count = _updateables.Count; i < count; i++ )
		{
			Updateable updateable = _updateables[i];

			updateable.Update();
		}
	}

	private void RegisterUpdatables()
	{
		_updateables = new List<Updateable>(10);

		foreach (Manager manager in _managers)
		{
			Updateable updatable = manager as Updateable;

			if (updatable == null)
				continue;

			_updateables.Add(updatable);
		}
	}

	private void InitialiseManagers()
	{
		var gameState = new GameState();

		_managers = new Manager[] 
		{ 
			new PaddleManager(this, gameState),

			new BallsManager(this, gameState),

			new LivesManager(this, gameState),

			new ScoreManager(this, gameState),

			new BricksManager(this, gameState),

			new EffectsManager(this, gameState),

			//new SoundsManager(this, gameData),

			new KeyInputManager(this, gameState),

			new PowerupManager(this, gameState),
		};
	}

	private void RegisterBallObservers()
	{
		var subject =  GetManager<BallsManager>() as Subject;

		foreach (Manager manager in _managers)
		{
			if (manager is BallObserver == false)
				continue;

			var observer = manager as BallObserver;

			subject.AddObserver(observer);
		}
	}

	private T GetManager<T>() where T : Manager
	{
		return (T) System.Array.Find(_managers, m => m.GetType() == typeof(T));
	}
}
