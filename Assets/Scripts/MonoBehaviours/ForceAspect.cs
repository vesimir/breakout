﻿using UnityEngine;

public class ForceAspect : MonoBehaviour
{
	[SerializeField]
	float _height = 16f;

	[SerializeField]
	float _width = 9f;

	void Start()
	{
		float scaleheight = GetScaleHeight();

		Camera camera = GetComponent<Camera>();

		if (scaleheight < 1.0f)
		{
			AddLetterBox(scaleheight, camera);
		}
		else
		{
			AddPillarBox(scaleheight, camera);
		}
	}

	private float GetScaleHeight()
	{
		float targetaspect = _height / _width;

		float windowaspect = (float)Screen.width / (float)Screen.height;

		float scaleheight = windowaspect / targetaspect;
		return scaleheight;
	}

	private static void AddPillarBox(float scaleheight, Camera camera)
	{
		float scalewidth = 1.0f / scaleheight;

		Rect rect = camera.rect;

		rect.width = scalewidth;
		rect.height = 1.0f;
		rect.x = (1.0f - scalewidth) / 2.0f;
		rect.y = 0;

		camera.rect = rect;
	}

	private static void AddLetterBox(float scaleheight, Camera camera)
	{
		Rect rect = camera.rect;

		rect.width = 1.0f;
		rect.height = scaleheight;
		rect.x = 0;
		rect.y = (1.0f - scaleheight) / 2.0f;

		camera.rect = rect;
	}
}
