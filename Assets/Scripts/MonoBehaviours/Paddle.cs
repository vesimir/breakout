using UnityEngine;

[RequireComponent(typeof(BoxCollider2D))]
public class Paddle : MonoBehaviour
{
	[SerializeField]
	float _initialSpeed;

	public float Speed { get; private set; }

	BoxCollider2D _collider;

	void Awake()
	{
		_collider = GetComponent<BoxCollider2D>();

		Speed = _initialSpeed;
	}

	public void SetPostion(Vector2 pos)
	{
		transform.position = pos;
	}

	public Vector2 GetPosition()
	{
		return transform.position;
	}

	public Vector2 GetColliderSize()
	{
		return _collider.size;
	}
}
